from HCRP_LM.ddHCRP_LM import *
import time

np.random.seed(42)

# TEST 1 - uniform sequence

print('--------------')
print('TEST 1 STARTED')
np.random.seed(42)
start_time = time.time()
m = HCRP_LM([1,1,1,1,1])
sequence = [0]*1000
m.fit(sequence)
if np.array_equal(m.choice_probs, [1]*1000):
    print(f'TEST 1 PASSED IN {time.time()-start_time} S')
else:
    print('TEST 1 FAILED')

# TEST 2 - random sequence

print('--------------')
print('TEST 2 STARTED')

np.random.seed(42)
test_2_probs = np.load('test_2_probs.npy')
start_time = time.time()
m = HCRP_LM([1,1,1,1,1])
sequence = np.random.choice(range(5),1000).tolist()
m.fit(sequence)
if np.array_equal(np.round(m.choice_probs, 3), test_2_probs):
    print(f'TEST 2 PASSED IN {time.time()-start_time} S')
else:
    print('TEST 2 FAILED')

# TEST 3 - forgetful model (more computation needed because timestamps need to be tracked)

print('--------------')
print('TEST 3 STARTED')

np.random.seed(42)
test_3_probs = np.load('test_3_probs.npy')
start_time = time.time()
m = HCRP_LM(strength=[1,1,1,1,1], decay_constant=[100,100,100,100,100])
sequence = np.random.choice(range(5),1000).tolist()
m.fit(sequence)
if np.array_equal(np.round(m.choice_probs, 3), test_3_probs):
    print(f'TEST 3 PASSED IN {time.time()-start_time} S')
else:
    print('TEST 3 FAILED')
